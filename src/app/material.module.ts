import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatInputModule} from '@angular/material/input';
import {MatChipsModule} from '@angular/material/chips';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        MatInputModule,
        MatChipsModule,
        MatDialogModule,
        MatIconModule,
        MatDatepickerModule
    ],
    providers: [
    ],
    exports: [
        CommonModule,
        MatInputModule,
        MatChipsModule,
        MatDialogModule,
        MatIconModule,
        MatDatepickerModule
    ]
})
export class MaterialModule {
}
