import {Injectable} from '@angular/core';
import {KeyValue} from '@angular/common';

export interface EsitiChips {
    label: number;
    value: string;
    checked: boolean;
}

export interface Causali {
    label: number;
    value: string;
    checked: boolean;
}

export interface RangeDate {
    label: number;
    value: string;
    checked: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class LtmSearchbarMobileService {

    public miniGames = false;

    public causaliList: Causali[] = [
        {label: -1, value: 'Tutte le causali', checked: true},
        {label: 1, value: 'Bonus', checked: false},
        {label: 2, value: 'Deposito', checked: false},
        {label: 3, value: 'Giocata Bingo', checked: false},
        {label: 4, value: 'Giocata Casino', checked: false},
        {label: 5, value: 'Giocata Eurojackpot', checked: false},
        {label: 6, value: 'Causale 1', checked: false},
        {label: 7, value: 'Causale 2', checked: false},
        {label: 8, value: 'Causale 3', checked: false},
        {label: 9, value: 'Causale 4', checked: false},
        {label: 10, value: 'Causale 5', checked: false},
        {label: 11, value: 'Causale 6', checked: false},
        {label: 12, value: 'Causale 7', checked: false},
        {label: 13, value: 'Causale 8', checked: false}
    ];

    public esitiChips: EsitiChips[] = [
        {label: -1, value: 'Tutti', checked: false},
        {label: 1, value: 'Vincente', checked: false},
        {label: 2, value: 'Non vincente', checked: false},
        {label: 3, value: 'In corso', checked: false},
        {label: 4, value: 'Rimborsato', checked: false},
        {label: 5, value: 'Non rimborsato', checked: false}
    ];

    public rangeTemporale: RangeDate[] = [
        {label: 1, value: 'Ultime 24 ore', checked: false},
        {label: 2, value: 'Ultimi 7 giorni', checked: false},
        {label: 3, value: 'Ultimi 30 giorni', checked: false},
        {label: 4, value: 'Ultimi 3 mesi', checked: false},
        {label: 5, value: 'Ultimi 6 mesi', checked: false},
        {label: 6, value: '*Ultimi 12 mesi', checked: false},
    ];

    endDate = null;
    startDate = null;

    constructor() {
    }

    resetFilters() {
        this.causaliList = [
            {label: -1, value: 'Tutte le causali', checked: true},
            {label: 1, value: 'Bonus', checked: false},
            {label: 2, value: 'Deposito', checked: false},
            {label: 3, value: 'Giocata Bingo', checked: false},
            {label: 4, value: 'Giocata Casino', checked: false},
            {label: 5, value: 'Giocata Eurojackpot', checked: false},
            {label: 6, value: 'Causale 1', checked: false},
            {label: 7, value: 'Causale 2', checked: false},
            {label: 8, value: 'Causale 3', checked: false},
            {label: 9, value: 'Causale 4', checked: false},
            {label: 10, value: 'Causale 5', checked: false},
            {label: 11, value: 'Causale 6', checked: false},
            {label: 12, value: 'Causale 7', checked: false},
            {label: 13, value: 'Causale 8', checked: false}
        ];

        this.esitiChips = [
            {label: -1, value: 'Tutti', checked: false},
            {label: 1, value: 'Vincente', checked: false},
            {label: 2, value: 'Non vincente', checked: false},
            {label: 3, value: 'In corso', checked: false},
            {label: 4, value: 'Rimborsato', checked: false},
            {label: 5, value: 'Non rimborsato', checked: false}
        ];

        this.rangeTemporale = [
            {label: 1, value: 'Ultime 24 ore', checked: false},
            {label: 2, value: 'Ultimi 7 giorni', checked: false},
            {label: 3, value: 'Ultimi 30 giorni', checked: false},
            {label: 4, value: 'Ultimi 3 mesi', checked: false},
            {label: 5, value: 'Ultimi 6 mesi', checked: false},
            {label: 6, value: '*Ultimi 12 mesi', checked: false},
        ];

        this.miniGames = false;

        this.endDate = this.startDate = null;
    }
}
