import {Component, HostListener, OnInit, ViewEncapsulation} from '@angular/core';
import {EsitiChips, LtmSearchbarMobileService} from '../core/services/ltm-searchbar-mobile.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {DatepikerDialogMobileComponent} from './datepiker-dialog-mobile/datepiker-dialog-mobile.component';
import {FormBuilder, FormGroup} from '@angular/forms';
import moment from 'moment';

@Component({
    selector: 'app-ltm-searchbar-mobile',
    templateUrl: './ltm-searchbar-mobile.component.html',
    styleUrls: ['./ltm-searchbar-mobile.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LtmSearchbarMobileComponent implements OnInit {

    showFilter = '';
    dateForm: FormGroup;
    public innerWidth: any;

    constructor(
        public ltmSearchbarMobileService: LtmSearchbarMobileService,
        private router: Router,
        public dialog: MatDialog,
        private fb: FormBuilder
    ) {
        this.ltmSearchbarMobileService.causaliList.map(res => {
            if (res && res.checked) {
                this.showFilter += res.value + ', ';
            }
        });

        this.showFilter = this.showFilter.substring(0, this.showFilter.length - 2);

        if (this.showFilter === '') {
            this.showFilter = this.ltmSearchbarMobileService.causaliList[0].value;
        }
    }

    ngOnInit(): void {
        this.innerWidth = window.innerWidth;

        this.dateForm = this.fb.group({
            data1: [],
            data2: [],
        });
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.innerWidth = window.innerWidth;
    }

    get dateActive() {
        return this.ltmSearchbarMobileService.endDate && this.ltmSearchbarMobileService.startDate;
    }

    get disabledImpostaFiltri() {
        let dateActive = false;
        dateActive = this.ltmSearchbarMobileService.endDate && this.ltmSearchbarMobileService.startDate;
        return this.ltmSearchbarMobileService.esitiChips.filter(el => el.checked).length === 0 && !dateActive &&
            this.ltmSearchbarMobileService.rangeTemporale.filter(el => el.checked).length === 0;
    }

    goToCausali() {
        const causaliOrdered =
            this.ltmSearchbarMobileService.causaliList.sort((a, b) => a.label > b.label ? 1 : a.label < b.label ? -1 : 0);
        this.router.navigate(['ltm-searchbar-mobile/causale'],
            {queryParams: {causaliList: JSON.stringify(causaliOrdered)}}).finally();
    }

    removeFilters() {
        this.ltmSearchbarMobileService.resetFilters();
        this.ltmSearchbarMobileService.causaliList.map(el => {
            if (el.checked) {
                this.showFilter = el.value;
            }
        });
    }

    resetDates() {
        this.ltmSearchbarMobileService.startDate = null;
        this.ltmSearchbarMobileService.endDate = null;
    }

    selectRange(index) {

        if (this.dateActive) {
            this.resetDates();
        }

        if (this.ltmSearchbarMobileService.rangeTemporale[index].checked) {
            this.ltmSearchbarMobileService.rangeTemporale[index].checked = false;
        } else {
            this.ltmSearchbarMobileService.rangeTemporale.map(el => {
                el.checked = false;
            });

            this.ltmSearchbarMobileService.rangeTemporale[index].checked = true;
        }
    }

    isSelected(index) {
        return this.ltmSearchbarMobileService.rangeTemporale[index].checked;
    }

    customDate() {

    }

    onClickPersonalizza() {
        const dialogRef = this.dialog.open(DatepikerDialogMobileComponent, {
            width: 'auto',
            height: 'auto',
            autoFocus: false,
            data: {
                startDate: this.ltmSearchbarMobileService.startDate,
                endDate: this.ltmSearchbarMobileService.endDate
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result !== undefined) {
                if (result.startDate !== undefined && result.endDate !== undefined) {
                    this.ltmSearchbarMobileService.startDate = result.startDate;
                    this.ltmSearchbarMobileService.endDate = result.endDate;

                    this.ltmSearchbarMobileService.rangeTemporale.map(el => {
                        el.checked = false;
                    });
                }
            }
        });
    }

    onClickEsiti(chip: EsitiChips) {
        if (chip.label === -1) {
            this.ltmSearchbarMobileService.esitiChips.filter(el => el.label !== -1).forEach(el => {
                el.checked = false;
            });
        } else {
            this.ltmSearchbarMobileService.esitiChips.find(el => el.label === -1).checked = false;
        }
        chip.checked = !chip.checked;
    }

    checkMinigames() {
        this.ltmSearchbarMobileService.miniGames = !this.ltmSearchbarMobileService.miniGames;
    }

    get filterDiff() {
        return this.ltmSearchbarMobileService.rangeTemporale.filter(el => el.checked).length === 0 &&
            this.ltmSearchbarMobileService.esitiChips.filter(el => el.checked).length === 0 &&
            this.ltmSearchbarMobileService.causaliList.filter(el => el.checked && el.label !== -1).length === 0 &&
            this.ltmSearchbarMobileService.startDate === null && this.ltmSearchbarMobileService.endDate === null &&
            this.ltmSearchbarMobileService.miniGames === false;
    }

    onChangeStartDate(startDate) {
        if (startDate) {
            if (startDate.length === 2) {
                this.ltmSearchbarMobileService.startDate += '/';
            } else if (startDate.length === 5) {
                this.ltmSearchbarMobileService.startDate += '/';
            }
        }
    }

    onChangeEndDate(endDate) {
        if (endDate) {
            if (endDate.length === 2) {
                this.ltmSearchbarMobileService.endDate += '/';
            } else if (endDate.length === 5) {
                this.ltmSearchbarMobileService.endDate += '/';
            }
        }
    }
}
