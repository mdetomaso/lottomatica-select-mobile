import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LtmSearchbarMobileComponent} from './ltm-searchbar-mobile.component';
import {RouterModule, Routes} from '@angular/router';
import {LtmSearchbarCausaleComponent} from './ltm-searchbar-causale/ltm-searchbar-causale.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DatepikerDialogMobileComponent} from './datepiker-dialog-mobile/datepiker-dialog-mobile.component';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import {MaterialModule} from '../material.module';
import {OnlynumberDirective} from '../core/directives/onlynumber.directive';

const routes: Routes = [
    {
        path: '',
        component: LtmSearchbarMobileComponent,
    },
    {
        path: 'causale',
        component: LtmSearchbarCausaleComponent
    }
];

@NgModule({
    declarations: [
        LtmSearchbarMobileComponent,
        LtmSearchbarCausaleComponent,
        DatepikerDialogMobileComponent,
        OnlynumberDirective
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        NgxDaterangepickerMd.forRoot(),
        MaterialModule,
        FormsModule
    ],
    exports: [
        RouterModule
    ],
    entryComponents: [
        DatepikerDialogMobileComponent
    ]
})
export class LtmSearchbarMobileModule {
}
