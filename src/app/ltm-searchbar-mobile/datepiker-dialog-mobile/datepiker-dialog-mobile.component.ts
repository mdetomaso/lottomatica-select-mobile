import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import moment from 'moment';
import {DaterangepickerComponent, LocaleConfig} from 'ngx-daterangepicker-material';
import {distinctUntilChanged} from 'rxjs/operators';

@Component({
    selector: 'app-datepiker-dialog-mobile',
    templateUrl: './datepiker-dialog-mobile.component.html',
    styleUrls: ['./datepiker-dialog-mobile.component.scss']
})
export class DatepikerDialogMobileComponent implements OnInit {

    dateForm: FormGroup;
    startDate = '';
    endDate = '';
    endDateIsBeforeStartDate = false;
    endDateIsBetweenStartAndToday = true;
    startDateIsAfterToday = false;
    endDateIsAfterToday = false;
    errorValidityStartDate = false;
    errorValidityEndDate = false;

    today = moment(new Date());

    startDateMoment = moment(new Date());
    endDateMoment = moment(new Date());

    localeOptions = {
        direction: 'ltr',
        weekLabel: 'W',
        daysOfWeek: [
            'D',
            'L',
            'M',
            'M',
            'G',
            'V',
            'S'
        ],
        firstDay: 1,
        monthNames: [
            'GENNAIO',
            'FEBBRAIO',
            'MARZO',
            'APRILE',
            'MAGGIO',
            'GIUGNO',
            'LUGLIO',
            'AGOSTO',
            'SETTEMBRE',
            'OTTOBRE',
            'NOVEMBRE',
            'DICEMBRE'
        ]
    } as LocaleConfig;

    constructor(
        public dialogRef: MatDialogRef<DatepikerDialogMobileComponent>,
        private fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private cdr: ChangeDetectorRef
    ) {
        moment().locale('it');

        if (this.data.startDate && this.data.endDate) {
            const dateStart = this.data.startDate.split('/');
            const dateEnd = this.data.endDate.split('/');
            this.startDateMoment = moment([+dateStart[2], +dateStart[1] - 1, +dateStart[0]]);
            this.endDateMoment = moment([+dateEnd[2], +dateEnd[1] - 1, +dateEnd[0]]);

            this.startDate = this.data.startDate;
            this.endDate = this.data.endDate;
        }

        this.dateForm = this.fb.group({
            startDate: [this.data.startDate],
            endDate: [this.data.endDate],
        });
    }

    close(endDate?: string, startDate?: string): void {
        const responseBody = {startDate, endDate};
        this.dialogRef.close(responseBody);
    }

    ngOnInit(): void {
    }

    choosedDate(evento: any) {
        if (evento.chosenLabel) {
            let dal = evento.chosenLabel.slice(0, 10);
            let al = evento.chosenLabel.slice(13);
            dal = dal.slice(3, 6) + dal.slice(0, 3) + dal.slice(6);
            al = al.slice(3, 6) + al.slice(0, 3) + al.slice(6);
            this.startDate = dal;
            this.endDate = al;

            const dateStart = this.startDate.split('/');
            this.startDateMoment = moment([+dateStart[2], +dateStart[1] - 1, +dateStart[0]]);
            this.startDateIsAfterToday = this.startDateMoment.isAfter(this.today);

            const dateEnd = this.endDate.split('/');
            this.endDateMoment = moment([+dateEnd[2], +dateEnd[1] - 1, +dateEnd[0]]);
            this.endDateIsAfterToday = this.endDateMoment.isAfter(this.today);
            if (this.startDateMoment) {
                this.endDateIsBetweenStartAndToday = this.endDateMoment.isBetween(this.startDateMoment, this.today, undefined, '[]');
            }
        }
    }

    get errorsValidity() {
        if (this.startDate && this.endDate) {
            if (this.startDate.length === 10 && this.endDate.length === 10) {
                if (!this.errorValidityStartDate && !this.errorValidityEndDate) {
                    return this.startDateIsAfterToday || this.endDateIsAfterToday || !this.endDateIsBetweenStartAndToday;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    startDateChange(event, calendar: DaterangepickerComponent) {
        if (this.endDate.length > 0) {
            this.endDate = '';
            // calendar.setEndDate(moment());
        }

        this.startDate = moment(event.startDate).format('DD/MM/YYYY');
    }

    endDateChange(event) {
    }

    onChangeStartDate(startDate) {
        if (startDate) {
            if (startDate.length === 2) {
                this.startDate += '/';
            } else if (startDate.length === 5) {
                this.startDate += '/';
            } else if (startDate.length === 10) {
                const dateStart = this.startDate.split('/');
                this.startDateMoment = moment([+dateStart[2], +dateStart[1] - 1, +dateStart[0]]);
                if (this.startDateMoment.isValid()) {
                    this.errorValidityStartDate = false;
                    this.startDateIsAfterToday = this.startDateMoment.isAfter(this.today);
                } else {
                    this.errorValidityStartDate = true;
                }
            }
        }
    }

    clickEnter(evt) {
        if (evt.key === 'Enter') {
            if (this.endDate.length === 10) {
                const dateEnd = this.endDate.split('/');
                const dateEndMoment = moment([+dateEnd[2], +dateEnd[1] - 1, +dateEnd[0]]);
                if (dateEndMoment.isValid()) {
                    if (dateEndMoment.isBefore(this.today) && dateEndMoment.isBetween(this.startDateMoment, this.today, undefined, '[]')) {
                        const endDate = this.endDate;
                        const startDate = this.startDate;
                        const responseBody = {startDate, endDate};
                        this.dialogRef.close(responseBody);
                    }
                }
            }
        }
    }

    onChangeEndDate(endDate) {
        if (endDate) {
            if (endDate.length === 2) {
                this.endDate += '/';
            } else if (endDate.length === 5) {
                this.endDate += '/';
            } else if (endDate.length === 10) {
                const dateEnd = this.endDate.split('/');
                this.endDateMoment = moment([+dateEnd[2], +dateEnd[1] - 1, +dateEnd[0]]);
                if (this.endDateMoment.isValid()) {
                    this.endDateIsAfterToday = this.endDateMoment.isAfter(this.today);
                    if (this.startDateMoment) {
                        this.errorValidityEndDate = false;
                        this.endDateIsBetweenStartAndToday =
                            this.endDateMoment.isBetween(this.startDateMoment, this.today, undefined, '[]');
                    }
                } else {
                    this.errorValidityEndDate = true;
                }
            }
        }
    }

}
