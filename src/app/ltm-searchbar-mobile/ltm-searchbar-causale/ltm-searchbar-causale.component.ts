import {Component, OnInit} from '@angular/core';
import {Causali, LtmSearchbarMobileService} from '../../core/services/ltm-searchbar-mobile.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-ltm-searchbar-causale',
    templateUrl: './ltm-searchbar-causale.component.html',
    styleUrls: ['./ltm-searchbar-causale.component.scss']
})
export class LtmSearchbarCausaleComponent implements OnInit {

    causaliTmp: Causali[] = [];
    causaliFilter: Causali[] = [];
    filterSearch = '';
    filterSearchResult: Causali[] = [];

    filterChanged = false;
    filtersActiveId: Causali[] = [];

    causaliListFromStart: string;

    constructor(
        public ltmSearchbarMobileService: LtmSearchbarMobileService,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {

        this.activatedRoute.queryParams.subscribe(queryParams => {
            if (queryParams.causaliList) {
                this.causaliListFromStart = queryParams.causaliList;
            }
        });

        this.causaliTmp = [...this.ltmSearchbarMobileService.causaliList.filter(el => !el.checked)];
        this.causaliFilter = [...this.ltmSearchbarMobileService.causaliList.filter(el => el.checked)];

        this.filtersActiveId = this.ltmSearchbarMobileService.causaliList.filter(el => el.checked);
    }

    ngOnInit(): void {
    }

    selectCausale(causale: Causali) {
        causale.checked = !causale.checked;
        const causaleIndex = this.causaliTmp.findIndex(el => el.label === causale.label);
        if (causaleIndex > -1) {
            if (causale.label === -1) {
                this.causaliFilter.map(el => {
                    el.checked = false;
                    this.causaliTmp.push(el);
                });
                this.causaliFilter = [];
                this.causaliFilter.push(causale);
            } else {
                const findAllIndex = this.causaliFilter.findIndex(el => el.label === -1);
                if (findAllIndex > -1) {
                    this.causaliTmp.push({label: -1, value: 'Tutte le causali', checked: false});
                    this.causaliFilter.splice(findAllIndex, 1);
                }
                this.causaliFilter.push(causale);
            }
            this.causaliTmp.splice(causaleIndex, 1);
        } else {
            this.causaliTmp.push(causale);
            this.causaliFilter = this.causaliFilter.filter(el => el.label !== causale.label);
        }

        this.causaliFilter.sort((a, b) => a.label > b.label ? 1 : a.label < b.label ? -1 : 0);
        this.causaliTmp.sort((a, b) => a.label > b.label ? 1 : a.label < b.label ? -1 : 0);
    }

    filter(event) {
        this.filterSearchResult =
            this.mergeArrays().filter(el => el.value.toLocaleLowerCase().includes(this.filterSearch.toLocaleLowerCase()));
    }

    mergeArrays() {
        const arrayMerged = [];
        this.causaliTmp.forEach(el => {
            arrayMerged.push(el);
        });
        this.causaliFilter.forEach(el => {
            arrayMerged.push(el);
        });
        return arrayMerged;
    }

    resetFilter() {
        const resetFilterCausali = JSON.parse(this.causaliListFromStart);
        this.causaliTmp = [...resetFilterCausali.filter(el => !el.checked)];
        this.causaliFilter = [...resetFilterCausali.filter(el => el.checked)];

        const tempArray = [];
        this.causaliTmp.forEach(el => {
            tempArray.push(el);
        });
        this.causaliFilter.forEach(el => {
            tempArray.push(el);
        });

        this.ltmSearchbarMobileService.causaliList = [...tempArray];

        this.filtersActiveId = resetFilterCausali.filter(el => el.checked);
    }

    get filterDisabled() {
        this.ltmSearchbarMobileService.causaliList.sort((a, b) => a.label > b.label ? 1 : a.label < b.label ? -1 : 0);
        return this.causaliListFromStart === JSON.stringify(this.ltmSearchbarMobileService.causaliList);
    }

    sort() {

    }

    saveFilter() {
        const tempArray = [];
        this.causaliTmp.forEach(el => {
            tempArray.push(el);
        });
        this.causaliFilter.forEach(el => {
            tempArray.push(el);
        });

        this.ltmSearchbarMobileService.causaliList = [...tempArray];

        this.router.navigate(['/ltm-searchbar-mobile'], {queryParams: {filterSaved: true}}).finally();
    }

    goBack() {
        this.ltmSearchbarMobileService.causaliList = [...JSON.parse(this.causaliListFromStart)];
        this.router.navigate(['/ltm-searchbar-mobile'], {queryParams: {filterSaved: false}}).finally();
    }

}
