import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/ltm-searchbar-mobile',
        pathMatch: 'full'
    },
    {
        path: 'ltm-searchbar-mobile',
        loadChildren: './ltm-searchbar-mobile/ltm-searchbar-mobile.module#LtmSearchbarMobileModule',
    },
    {
        path: 'ltm-searchbar-mobile/causale',
        loadChildren: './ltm-searchbar-mobile/ltm-searchbar-mobile.module#LtmSearchbarMobileModule'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
